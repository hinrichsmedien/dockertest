pipeline {
  triggers { pollSCM('* * * * *') }

  environment {
    imageName = "matthias/myapp"
    dockerImage = ""
  }

  agent { label 'docker' }
  
  options {
    skipDefaultCheckout true
  }

  stages {
    
    stage('Checkout Source') {
      steps {
        git poll: true,
            url: 'https://bitbucket.org/hinrichsmedien/dockertest.git'
      }
    }

    stage('Build image') {
      steps{
        script {
          dockerImage = docker.build imageName + ":$BUILD_NUMBER"
        }
      }
    }

    stage('Push Image') {
      steps{
        script {
          docker.withRegistry( "https://registry.hnrx.de", 'portus_creds_matthias' ) {
            dockerImage.push()
            dockerImage.push('latest')
          }
        }
      }
    }  

    stage('Remove local images') {
      steps {
        // remove docker images
        sh("docker rmi -f registry.hnrx.de/matthias/myapp:latest || :")
        sh("docker rmi -f registry.hnrx.de/matthias/myapp:$BUILD_NUMBER || :")
        sh("docker rmi -f matthias/myapp:$BUILD_NUMBER || :")
      }
    }
    
  }

  post {
         unstable {
             echo 'This will run only if the run was marked as unstable'
         }
         changed {
             echo 'This will run only if the state of the Pipeline has changed'
             echo 'For example, if the Pipeline was previously failing but is now successful'
         }
     }
}
